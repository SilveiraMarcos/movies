import axios from "axios";

const API_KEY = 'eb4f4c49c25a829d9ec87b56fbf8abac';
const BASE_URL = 'https://api.themoviedb.org/3/';
const withBaseUrl = path => `${BASE_URL}${path}?api_key=${API_KEY}`;

export class MoviesService{
    static getMovies(){
        return axios(withBaseUrl('movie/popular'));
    }

    static getMoviesById(id){
        return axios(withBaseUrl(`movie/${id}`));
    }
}